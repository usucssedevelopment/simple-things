﻿using System.Collections;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Things;

namespace ThingsTesting
{
    [TestClass]
    public class ThingABobCollectionTest
    {
        [TestMethod]
        public void ThingABobCollection_TestConstructionAndInsertion()
        {
            var thing1 = new Gadget() {Id = 1, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25};
            var thing2 = new Gadget() {Id = 2, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25};
            var thing3 = new Gadget() {Id = 3, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25};
            var thing4 = new Widget() {Id = 4, UnitCost = 10, Weight = 30, Markup = 1.5};
            var thing5 = new Widget() {Id = 5, UnitCost = 10, Weight = 30, Markup = 1.5};

            var collection = new ThingABobCollection();
            Assert.AreEqual(0, collection.Count);
            collection.Add(thing1);
            Assert.AreEqual(1, collection.Count);
            collection.Add(thing2);
            Assert.AreEqual(2, collection.Count);
            collection.Add(thing3);
            Assert.AreEqual(3, collection.Count);
            collection.Add(thing4);
            Assert.AreEqual(4, collection.Count);
            collection.Add(thing5);
            Assert.AreEqual(5, collection.Count);

            collection.Add(null);
            Assert.AreEqual(5, collection.Count);

            collection.Add(thing1);
            Assert.AreEqual(5, collection.Count);
            collection.Add(thing3);
            Assert.AreEqual(5, collection.Count);
            collection.Add(thing5);
            Assert.AreEqual(5, collection.Count);
        }

        [TestMethod]
        public void ThingABobCollection_TestReadWrite()
        {
            File.Delete("test-ThingABobCollection.json");

            var thing1 = new Gadget() {Id = 1, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25};
            var thing2 = new Gadget() {Id = 2, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25};
            var thing3 = new Gadget() {Id = 3, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25};
            var thing4 = new Widget() {Id = 4, UnitCost = 10, Weight = 30, Markup = 1.5};
            var thing5 = new Widget() {Id = 5, UnitCost = 10, Weight = 30, Markup = 1.5};

            var collection = new ThingABobCollection() {thing1, thing2, thing3, thing4, thing5};
            Assert.AreEqual(5, collection.Count);

            collection.Write("test-ThingABobCollection.json");
            Assert.IsFalse(File.Exists("test-ThingABobCollection.json"));

            collection.MyImporterExporter = new JsonReaderWriter();
            collection.Write("test-ThingABobCollection.json");
            Assert.IsTrue(File.Exists("test-ThingABobCollection.json"));

            var collection2 = new ThingABobCollection() {MyImporterExporter = new JsonReaderWriter()};
            collection2.Read("test-ThingABobCollection.json");
            Assert.AreEqual(5, collection.Count);
        }

        [TestMethod]
        public void ThingABobCollection_TestEnumerators()
        {
            var thing1 = new Gadget() { Id = 1, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25 };
            var thing2 = new Gadget() { Id = 2, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25 };
            var thing3 = new Gadget() { Id = 3, Length = 10, Width = 20, PerAreaUnitCost = 0.5, Markup = 1.25 };
            var thing4 = new Widget() { Id = 4, UnitCost = 10, Weight = 30, Markup = 1.5 };
            var thing5 = new Widget() { Id = 5, UnitCost = 10, Weight = 30, Markup = 1.5 };

            var collection = new ThingABobCollection() { thing1, thing2, thing3, thing4, thing5 };
            Assert.IsNotNull(collection.GetEnumerator());
            Assert.IsNotNull(((IEnumerable) collection).GetEnumerator());

            foreach (var thingy in collection)
                Assert.IsTrue(thingy.Id>0);
        }
    }
}
