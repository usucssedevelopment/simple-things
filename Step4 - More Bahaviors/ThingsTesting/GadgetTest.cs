﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Things;

namespace ThingsTesting
{
    [TestClass]
    public class GadgetTest
    {
        [TestMethod]
        public void Gadget_Construction()
        {
            // Test case - Zeros for inputs
            var g = new Gadget() { Id=0, PerAreaUnitCost = 0, Length = 0, Width = 0, Markup = 0};
            Assert.AreEqual(0, g.Id);
            Assert.AreEqual(0, g.PerAreaUnitCost);
            Assert.AreEqual(0, g.Length);
            Assert.AreEqual(0, g.Width);
            Assert.AreEqual(0, g.Markup);
            Assert.AreEqual(0, g.ShippingCost);
            Assert.AreEqual(0, g.UnitCost);
            Assert.AreEqual(0, g.Price);


            // Test case - Positive numbers for inputs
            g = new Gadget() { Id=1, PerAreaUnitCost = 1.5, Length = 2.6, Width = 3.4, Markup = 0.02 };
            Assert.AreEqual(1, g.Id);
            Assert.AreEqual(1.5, g.PerAreaUnitCost);
            Assert.AreEqual(2.6, g.Length);
            Assert.AreEqual(3.4, g.Width);
            Assert.AreEqual(0.02, g.Markup);
            Assert.AreEqual(1 + (2.6 * 3.4 * 0.05), g.ShippingCost, 0.0001);
            Assert.AreEqual(1.5 * 2.6 * 3.4, g.UnitCost, 0.0001);
            Assert.AreEqual((1.5 * 2.6 * 3.4 * 0.02) + 1 + (2.6 * 3.4 * 0.05), g.Price, 0.0001);


            // Test case - Negative for inputs
            g = new Gadget() { Id=-1, PerAreaUnitCost = -0.2, Length = -1.3, Width = -4.5, Markup = -0.01};
            Assert.AreEqual(-1, g.Id);
            Assert.AreEqual(0, g.PerAreaUnitCost);
            Assert.AreEqual(0, g.Length);
            Assert.AreEqual(0, g.Width);
            Assert.AreEqual(0, g.Markup);
            Assert.AreEqual(0, g.ShippingCost);
            Assert.AreEqual(0, g.UnitCost);
            Assert.AreEqual(0, g.Price);
        }

        [TestMethod]
        public void Gadget_TestGettersAndSetters()
        {
            var g = new Gadget() { PerAreaUnitCost = 1.5, Length = 25, Width = 4, Markup = 1};
            Assert.AreEqual(1.5, g.PerAreaUnitCost);
            Assert.AreEqual(25, g.Length);
            Assert.AreEqual(4, g.Width);
            Assert.AreEqual(1, g.Markup);
            Assert.AreEqual(6, g.ShippingCost);
            Assert.AreEqual(150, g.UnitCost, 0.0001);
            Assert.AreEqual(156, g.Price, 0.0001);


            g.PerAreaUnitCost = 3.25;
            Assert.AreEqual(3.25, g.PerAreaUnitCost);
            Assert.AreEqual(25, g.Length);
            Assert.AreEqual(4, g.Width);
            Assert.AreEqual(1, g.Markup);
            Assert.AreEqual(6, g.ShippingCost);
            Assert.AreEqual(325, g.UnitCost, 0.0001);
            Assert.AreEqual(331, g.Price, 0.0001);

            g.Length = 10;
            g.Width = 20;
            Assert.AreEqual(3.25, g.PerAreaUnitCost);
            Assert.AreEqual(10, g.Length);
            Assert.AreEqual(20, g.Width);
            Assert.AreEqual(1, g.Markup);
            Assert.AreEqual(11, g.ShippingCost);
            Assert.AreEqual(650, g.UnitCost, 0.0001);
            Assert.AreEqual(661, g.Price, 0.0001);

            g.Markup = 2.0;
            Assert.AreEqual(3.25, g.PerAreaUnitCost);
            Assert.AreEqual(10, g.Length);
            Assert.AreEqual(20, g.Width);
            Assert.AreEqual(2, g.Markup);
            Assert.AreEqual(11, g.ShippingCost);
            Assert.AreEqual(650, g.UnitCost, 0.0001);
            Assert.AreEqual(1311, g.Price, 0.0001);
        }

        [TestMethod]
        public void Gadget_TestWithZeroLength()
        {
            var g = new Gadget() { PerAreaUnitCost = 1.0, Width = 2, Markup = 1};
            Assert.AreEqual(0, g.ShippingCost);
            Assert.AreEqual(0, g.UnitCost);
            Assert.AreEqual(0, g.Price, 0.0001);
        }

        [TestMethod]
        public void Gadget_TestWithZeroWidth()
        {
            var g = new Gadget() { PerAreaUnitCost = 1.0, Length = 2, Markup = 1 };
            Assert.AreEqual(0, g.ShippingCost);
            Assert.AreEqual(0, g.UnitCost);
            Assert.AreEqual(0, g.Price, 0.0001);
        }

        [TestMethod]
        public void Gadget_TestWithZeroPerSquareInchCost()
        {
            var g = new Gadget() { Length = 20, Width = 5, Markup = 1 };
            Assert.AreEqual(6, g.ShippingCost, 0.0001);
            Assert.AreEqual(0, g.UnitCost);
            Assert.AreEqual(6, g.Price, 0.0001);
        }

        [TestMethod]
        public void Gadget_TestWithNonZeroParameters()
        {
            var g = new Gadget() { PerAreaUnitCost = 2.5, Length = 5, Width = 20, Markup = 1};
            Assert.AreEqual(6, g.ShippingCost, 0.0001);
            Assert.AreEqual(250, g.UnitCost, 0.0001);
            Assert.AreEqual(256, g.Price, 0.0001);

            g = new Gadget() { PerAreaUnitCost = 2.5, Length = 4, Width = 10, Markup = 1.25 };
            Assert.AreEqual(3, g.ShippingCost, 0.0001);
            Assert.AreEqual(100, g.UnitCost, 0.0001);
            Assert.AreEqual(128, g.Price, 0.0001);

        }

        [TestMethod]
        public void Gadget_TestSettingOfUnitCost()
        {
            var g = new Gadget() { PerAreaUnitCost = 2.5, Length = 5, Width = 20, Markup = 1};
            Assert.AreEqual(2.5, g.PerAreaUnitCost);
            Assert.AreEqual(5, g.Length);
            Assert.AreEqual(20, g.Width);
            Assert.AreEqual(6, g.ShippingCost, 0.0001);
            Assert.AreEqual(250, g.UnitCost, 0.0001);

            g.UnitCost = 200;
            Assert.AreEqual(2.0, g.PerAreaUnitCost, 0.0001);
            Assert.AreEqual(5, g.Length);
            Assert.AreEqual(20, g.Width);
            Assert.AreEqual(6, g.ShippingCost, 0.0001);
            Assert.AreEqual(200, g.UnitCost, 0.0001);

            g.Length = 0;
            Assert.AreEqual(2.0, g.PerAreaUnitCost);
            Assert.AreEqual(0, g.Length);
            Assert.AreEqual(20, g.Width);
            Assert.AreEqual(0, g.ShippingCost, 0.0001);
            Assert.AreEqual(0, g.UnitCost, 0.0001);

            g.UnitCost = 0;
            Assert.AreEqual(0, g.PerAreaUnitCost);
            Assert.AreEqual(0, g.Length);
            Assert.AreEqual(20, g.Width);
            Assert.AreEqual(0, g.ShippingCost, 0.0001);
            Assert.AreEqual(0, g.UnitCost, 0.0001);

            g.Length = 5;
            g.PerAreaUnitCost = 2.5;
            Assert.AreEqual(2.5, g.PerAreaUnitCost);
            Assert.AreEqual(5, g.Length);
            Assert.AreEqual(20, g.Width);
            Assert.AreEqual(6, g.ShippingCost, 0.0001);
            Assert.AreEqual(250, g.UnitCost, 0.0001);

            g.Width = 0;
            Assert.AreEqual(2.5, g.PerAreaUnitCost);
            Assert.AreEqual(5, g.Length);
            Assert.AreEqual(0, g.Width);
            Assert.AreEqual(0, g.ShippingCost, 0.0001);
            Assert.AreEqual(0, g.UnitCost, 0.0001);

            g.UnitCost = 0;
            Assert.AreEqual(0, g.PerAreaUnitCost);
            Assert.AreEqual(5, g.Length);
            Assert.AreEqual(0, g.Width);
            Assert.AreEqual(0, g.ShippingCost, 0.0001);
            Assert.AreEqual(0, g.UnitCost, 0.0001);
        }
    }
}
