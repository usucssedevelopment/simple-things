﻿using System;
using System.Runtime.Serialization;


namespace Things
{
    [DataContract]
    public class Widget : ThingABob
    {
        private double _weight;

        [DataMember]
        public double Weight
        {
            get { return _weight; }
            set { _weight = Math.Max(0, value); }
        }

        public override double ShippingCost
        {
            get
            {
                double result = 0;
                if (Weight > 200)
                    result = Weight * 1.5;
                else if (Weight > 100)
                    result = .05;
                else if (Weight > 0)
                    result = .02;
                return result;
            }
        }
    }
}
