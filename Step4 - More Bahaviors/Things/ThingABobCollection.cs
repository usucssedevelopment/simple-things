﻿using System.Collections;
using System.Collections.Generic;

namespace Things
{
    public class ThingABobCollection : IEnumerable<ThingABob>
    {
        private List<ThingABob> _myCollection = new List<ThingABob>();
        public int Count => _myCollection.Count;
        public IReaderWriter MyImporterExporter { get; set; }

        public void Add(ThingABob thingy)
        {
            if (thingy==null || _myCollection.Contains(thingy)) return;

            _myCollection.Add(thingy);
        }

        public void Write(string filename)
        {
            MyImporterExporter?.Write(_myCollection, filename);
        }

        public void Read(string filename)
        {
            if (MyImporterExporter != null)
                _myCollection = MyImporterExporter.Read(filename);
        }

        public IEnumerator<ThingABob> GetEnumerator()
        {
            return _myCollection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _myCollection.GetEnumerator();
        }
    }
}
