﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace Things
{
    public class JsonReaderWriter : IReaderWriter
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(List<ThingABob>),
                     new [] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });


        public void Write(List<ThingABob> data, string filename)
        {
            if (data==null) 
                throw new ApplicationException("Cannot write out a null collection");

            if (string.IsNullOrWhiteSpace(filename))
                throw new ApplicationException("The output filename cannot be null or empty");

            try
            {
                var fileAttributes = File.GetAttributes(filename);
                if (fileAttributes.HasFlag(FileAttributes.Directory))
                    throw new ApplicationException($"{filename} is a directory");
            }
            catch (FileNotFoundException)
            {
                // Ignore -- this is 
            }

            if (!filename.ToLower().EndsWith(".json"))
                filename += ".json";

            try
            {
                var fileAttributes = File.GetAttributes(filename);
                if (fileAttributes.HasFlag(FileAttributes.Directory))
                    throw new ApplicationException($"{filename} is a directory");

            }
            catch (FileNotFoundException)
            {
                // Ignore -- this is 
            }

            File.Delete(filename);

            var writer = new StreamWriter(filename);
            JsonSerializer.WriteObject(writer.BaseStream, data);
            writer.Close();
        }

        public List<ThingABob> Read(string filename)
        {
            if (!filename.ToLower().EndsWith(".json"))
                filename += ".json";

            var reader = new StreamReader(filename);
            var data = JsonSerializer.ReadObject(reader.BaseStream) as List<ThingABob>;
            return data;
        }
    }
}
