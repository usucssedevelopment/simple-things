﻿using System.Collections.Generic;

namespace Things
{
    public interface IReaderWriter
    {
        void Write(List<ThingABob> list, string filename);
        List<ThingABob> Read(string filename);
    }
}
