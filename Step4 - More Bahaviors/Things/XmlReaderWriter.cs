﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Things
{
    public class XmlReaderWriter : IReaderWriter
    {
        private static readonly XmlSerializer XmlSerializer = new XmlSerializer(typeof(List<ThingABob>),
                        new Type[] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });

        public void Write(List<ThingABob> data, string filename)
        {
            if (data == null)
                throw new ApplicationException("Cannot write out a null collection");

            if (string.IsNullOrWhiteSpace(filename))
                throw new ApplicationException("The output filename cannot be null or empty");

            try
            {
                var fileAttributes = File.GetAttributes(filename);
                if (fileAttributes.HasFlag(FileAttributes.Directory))
                    throw new ApplicationException($"{filename} is a directory");
            }
            catch (FileNotFoundException)
            {
                // Ignore -- this is 
            }

            if (!filename.ToLower().EndsWith(".xml"))
                filename += ".xml";

            try
            {
                var fileAttributes = File.GetAttributes(filename);
                if (fileAttributes.HasFlag(FileAttributes.Directory))
                    throw new ApplicationException($"{filename} is a directory");
            }
            catch (FileNotFoundException)
            {
                // Ignore -- this is 
            }

            File.Delete(filename);

            var writer = new StreamWriter(filename);
            XmlSerializer.Serialize(writer, data);
            writer.Close();
        }

        public List<ThingABob> Read(string filename)
        {
            if (!filename.ToLower().EndsWith(".xml"))
                filename += ".xml";

            var reader = new StreamReader(filename);
            var data = XmlSerializer.Deserialize(reader.BaseStream) as List<ThingABob>;
            return data;
        }
    }
}
