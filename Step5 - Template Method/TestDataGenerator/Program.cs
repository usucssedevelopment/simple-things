﻿using System;

using Things;

namespace TestDataGenerator
{
    public class Program
    {
        private static readonly IReaderWriter[] ReaderWriters = {
                            new JsonReaderWriter(),
                            new XmlReaderWriter()
                        };

        public static void Main(string[] args)
        {
            var selectedReaderWriter = DetermineFileFormat();
            if (selectedReaderWriter == null) return;

            // Create a collection, populate with sample data, and save it to a file
            var collection1 = new ThingABobCollection() { MyImporterExporter = selectedReaderWriter };

            LoadSampleGadgets(collection1);
            Console.WriteLine($"Number of objects: {collection1.Count}");

            collection1.Write("CollectionData");

            // Create a second collection and load it from the file just save.
            var collection2 = new ThingABobCollection() { MyImporterExporter = selectedReaderWriter };

            collection2.Read("CollectionData");
            Console.WriteLine($"Number of objects: {collection2.Count}");
            Console.Read();
        }

        private static IReaderWriter DetermineFileFormat()
        {
            IReaderWriter result = null;
            while (result == null)
            {
                Console.Write("Do you want to work with (1) JSON or (2) XML? ");
                var response = Console.ReadLine()?.Trim().ToUpper();
                if (string.IsNullOrEmpty(response) || response == "EXIT")
                    return null;

                if (response == "1" || response == "JSON" || response == "J")
                    result = ReaderWriters[0];
                else if (response == "2" || response == "XML" || response == "X")
                    result = ReaderWriters[1];
            }

            return result;
        }

        private static void LoadSampleGadgets(ThingABobCollection collection)
        {
            collection.Add(new Gadget() { Id = 50, PerAreaUnitCost = 10.99, Length = 3, Width = 4 });
            collection.Add(new Widget() { Id = 51, Weight = 20.5 });
            collection.Add(new Gadget() { Id = 52, PerAreaUnitCost = 12.99, Length = 4, Width = 4 });
            collection.Add(new Widget() { Id = 53, Weight = 30.55 });
            collection.Add(new Widget() { Id = 54, Weight = 50.25 });
            collection.Add(new Widget() { Id = 55, Weight = 60.19 });
        }

    }
}
