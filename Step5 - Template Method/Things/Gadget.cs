﻿using System;
using System.Runtime.Serialization;

namespace Things
{
    [DataContract]
    public class Gadget : ThingABob
    {
        private double _perSquareInchCost;
        private double _length;
        private double _width;

        public override double UnitCost
        {
            set
            {
                _perSquareInchCost = (Math.Abs(Length) < double.Epsilon || Math.Abs(Width) < double.Epsilon) ? 0 : value / (Length * Width);
                ComputeUnitCost();
            }
        }

        [DataMember]
        public double PerAreaUnitCost
        {
            get { return _perSquareInchCost; }
            set
            {
                _perSquareInchCost = Math.Max(0.0, value);
                ComputeUnitCost();
            }
        }

        [DataMember]
        public double Length
        {
            get { return _length; }
            set
            {
                _length = Math.Max(0.0, value);
                ComputeUnitCost();
            }
        }
        [DataMember]
        public double Width
        {
            get { return _width; }
            set
            {
                _width = Math.Max(0.0, value);
                ComputeUnitCost();
            }
        }

        public override double ShippingCost
        {
            get
            {
                double result = 0;
                if (Length > 0 && Width > 0)
                    result = 1 + Length * Width * .05;
                return result;
            }
        }

        protected void ComputeUnitCost()
        {
            UnitCost = _length * _width * _perSquareInchCost;
        }
    }
}
