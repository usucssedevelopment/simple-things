﻿using System;
using System.Runtime.Serialization;

namespace Things
{
    [DataContract]
    public abstract class ThingABob
    {
        private double _unitCost;
        private double _markUp;


        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public virtual double UnitCost
        {
            get { return _unitCost; }
            set { _unitCost = Math.Max(0, value); }
        }

        [DataMember]
        public double Markup
        {
            get { return _markUp; }
            set { _markUp = Math.Max(0, value); }
        }

        public abstract double ShippingCost { get; }

        public double Price => UnitCost * Markup + ShippingCost;
    }
}
