﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Things
{
    public abstract class ReaderWriter
    {
        protected StreamWriter Writer { get; set; }
        protected StreamReader Reader { get; set; }

        public Exception LastException { get; private set; }

        public void Write(List<ThingABob> data, string filename)
        {
            if (data == null)
                throw new ApplicationException("Cannot write out a null collection");

            MakeSureFileIsNotADirectory(filename);

            filename = ComputeFullFilename(filename);                  // Abstract step -- specialization will override

            MakeSureFileIsNotADirectory(filename);

            File.Delete(filename);

            if (!OpenWriter(filename)) return;                         // Virtual step -- specialization may override

            WriteList(data);                                           // Abstract step -- specialization will override
            Writer.Close();
        }

        public List<ThingABob> Read(string filename)
        {
            filename = ComputeFullFilename(filename);                  // Abstract step -- specialization will override
            if (!OpenRead(filename)) return null;                      // Abstract step -- specialization will override

            var result = ReadList();                                   // Virtual step -- specialization may override
            Reader.Close();
            return result;
        }

        protected void MakeSureFileIsNotADirectory(string filename)
        {
            try
            {
                var fileAttributes = File.GetAttributes(filename);
                if (fileAttributes.HasFlag(FileAttributes.Directory))
                    throw new ApplicationException($"{filename} is a directory");
            }
            catch (FileNotFoundException)
            {
                // Ignore -- this is 
            }
        }

        protected abstract string ComputeFullFilename(string filename); 
        protected virtual bool OpenWriter(string filename)
        {
            var result = false;
            try
            {
                Writer = new StreamWriter(filename);
                result = true;
            }
            catch (Exception e)
            {
                LastException = e;
            }

            return result;
        }
        protected abstract void WriteList(List<ThingABob> list);

        protected virtual bool OpenRead(string filename)
        {
            var result = false;
            try
            {
                Reader = new StreamReader(filename);
                result = true;
            }
            catch (Exception e)
            {
                LastException = e;
            }

            return result;
        }

        protected abstract List<ThingABob> ReadList();
    }
}
