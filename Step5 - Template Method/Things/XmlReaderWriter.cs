﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Things
{
    public class XmlReaderWriter : ReaderWriter
    {
        private static readonly XmlSerializer XmlSerializer = new XmlSerializer(typeof(List<ThingABob>),
                        new [] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });

        protected override string ComputeFullFilename(string filename)
        {
            return filename + ".xml";
        }

        protected override void WriteList(List<ThingABob> list)
        {
            XmlSerializer.Serialize(Writer, list);            
        }

        protected override List<ThingABob> ReadList()
        {
            return XmlSerializer.Deserialize(Reader.BaseStream) as List<ThingABob>;
        }
    }
}
