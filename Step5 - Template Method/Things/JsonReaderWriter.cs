﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;

namespace Things
{
    public class JsonReaderWriter : ReaderWriter
    {
        private static DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(List<ThingABob>),
                     new Type[] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });


        protected override string ComputeFullFilename(string filename)
        {
            if (!filename.ToLower().EndsWith(".json"))
                filename += ".json";
            return filename;
        }

        protected override void WriteList(List<ThingABob> list)
        {
            jsonSerializer.WriteObject(Writer.BaseStream, list);
        }

        protected override List<ThingABob> ReadList()
        {
            List<ThingABob> data = jsonSerializer.ReadObject(Reader.BaseStream) as List<ThingABob>;
            return data;            
        }
    }
}
