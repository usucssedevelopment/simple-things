﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Things;

namespace ThingsTesting
{
    [TestClass]
    public class WidgetTest
    {
        [TestMethod]
        public void Widget_TestConstruction()
        {
            // Test with 0 inputs
            var widget = new Widget() { UnitCost = 0, Weight = 0, Markup = 0};
            Assert.AreEqual(0, widget.UnitCost);
            Assert.AreEqual(0, widget.Weight);
            Assert.AreEqual(0, widget.Markup);

            // Test positive inputs
            widget = new Widget() { UnitCost = 20, Weight = 30, Markup = 1 };
            Assert.AreEqual(20, widget.UnitCost);
            Assert.AreEqual(30, widget.Weight);
            Assert.AreEqual(1, widget.Markup);

            // Test negative inputs
            widget = new Widget() { UnitCost = -10, Weight = -20, Markup = -2 };
            Assert.AreEqual(0, widget.UnitCost);
            Assert.AreEqual(0, widget.Weight);
            Assert.AreEqual(0, widget.Markup);
        }

        [TestMethod]
        public void Widget_TestWithZeroUnitCost()
        {
            // Test shipping cost when the weight is 0
            var widget = new Widget() { UnitCost = 0, Weight = 10, Markup = 1 };
            Assert.AreEqual(0, widget.UnitCost);
            Assert.AreEqual(10, widget.Weight);
            Assert.AreEqual(.02, widget.ShippingCost);
            Assert.AreEqual(0.02, widget.Price);
        }


        [TestMethod]
        public void Widget_TestWithZeroWeight()
        {
            // Test shipping cost when the weight is 0
            var widget = new Widget() { UnitCost = 10, Weight = 0, Markup = 1};
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(0, widget.Weight);
            Assert.AreEqual(0, widget.ShippingCost);
            Assert.AreEqual(10, widget.Price);
        }

        [TestMethod]
        public void Widget_TestWithZeroMarkup()
        {
            // Test shipping cost when the weight is 0
            var widget = new Widget() { UnitCost = 10, Weight = 10, Markup = 0 };
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(10, widget.Weight);
            Assert.AreEqual(0.02, widget.ShippingCost);
            Assert.AreEqual(0.02, widget.Price);
        }

        [TestMethod]
        public void Widget_TestWithLightWeights()
        {
            // Test shipping cost when the weight is >0 but <=100
            var widget = new Widget() { UnitCost = 10, Weight = 35, Markup = 1.5};
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(35, widget.Weight);
            Assert.AreEqual(0.02, widget.ShippingCost);
            Assert.AreEqual(15.02, widget.Price);

            widget = new Widget() { UnitCost = 10, Weight = 100, Markup = 1.5 };
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(100, widget.Weight);
            Assert.AreEqual(0.02, widget.ShippingCost);
            Assert.AreEqual(15.02, widget.Price);
        }

        [TestMethod]
        public void Widget_TestWithMediumWeights()
        {
            // Test shipping cost when the weight is >100 but <=200
            var widget = new Widget() { UnitCost = 10, Weight = 100.1, Markup = 1.5 };
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(100.1, widget.Weight);
            Assert.AreEqual(0.05, widget.ShippingCost);
            Assert.AreEqual(15.05, widget.Price);

            widget = new Widget() { UnitCost = 10, Weight = 150, Markup = 1.5 };
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(150, widget.Weight);
            Assert.AreEqual(0.05, widget.ShippingCost);
            Assert.AreEqual(15.05, widget.Price);

            widget = new Widget() { UnitCost = 10, Weight = 200, Markup = 1.5 }; ;
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(200, widget.Weight);
            Assert.AreEqual(0.05, widget.ShippingCost);
            Assert.AreEqual(15.05, widget.Price);
        }

        [TestMethod]
        public void Widget_TestWithHeavyWeights()
        {
            // Test shipping cost when the weight is >200
            var widget = new Widget() { UnitCost = 10, Weight = 200.1, Markup = 1.5 };
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(200.1, widget.Weight);
            Assert.AreEqual(200.1 * 1.5, widget.ShippingCost, 0.0001);
            Assert.AreEqual(15 + (200.1 * 1.5), widget.Price);

            widget = new Widget() { UnitCost = 10, Weight = 250, Markup = 1.5 };
            Assert.AreEqual(10, widget.UnitCost);
            Assert.AreEqual(250, widget.Weight);
            Assert.AreEqual(250 * 1.5, widget.ShippingCost, 0.0001);
            Assert.AreEqual(15 + (250 * 1.5), widget.Price);
        }

    }
}
