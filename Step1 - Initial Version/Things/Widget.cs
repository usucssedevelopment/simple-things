﻿using System.Runtime.Serialization;

namespace Things
{
    [DataContract]
    public class Widget : ThingABob
    {
        [DataMember]
        public double Weight { get; set; }
    }
}
