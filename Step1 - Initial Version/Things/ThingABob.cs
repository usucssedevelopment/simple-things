﻿using System.Runtime.Serialization;

namespace Things
{
    [DataContract]
    public class ThingABob
    {
        [DataMember]
        public int Id { get; set; }
    }
}
