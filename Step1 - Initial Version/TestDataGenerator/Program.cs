﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Xml.Serialization;

using Things;

namespace TestDataGenerator
{
    public class Program
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(List<ThingABob>),
                                new[] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });


        private static readonly XmlSerializer XmlSerializer = new XmlSerializer(typeof(List<ThingABob>),
                                new[] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });

        public static void Main(string[] args)
        {
            var data = CreateSampleThings();
            Console.WriteLine("Number of objects: {0}", data.Count);

            SaveAsJson(data);
            SaveAsXml(data);

            var data2 = ReadJson();
            Console.WriteLine("Number of json objects read back in: {0}", data2.Count);
            var data3 = ReadXml();
            Console.WriteLine("Number of xml objects read back in: {0}", data3.Count);

            Console.ReadKey();
        }

        private static List<ThingABob> CreateSampleThings()
        {
            var list = new List<ThingABob>
            {
                new Gadget() {Id = 50, Cost = 10.99, Length = 3, Width = 4},
                new Widget() {Id = 51, Weight = 20.5},
                new Gadget() {Id = 52, Cost = 12.99, Length = 4, Width = 4},
                new Widget() {Id = 53, Weight = 30.55},
                new Widget() {Id = 54, Weight = 50.25},
                new Widget() {Id = 55, Weight = 60.19}
            };


            return list;
        }

        private static void SaveAsJson(List<ThingABob> data)
        {
            var writer = new StreamWriter(@"../../SampleData.json");
            JsonSerializer.WriteObject(writer.BaseStream, data);
            writer.Close();
        }

        private static void SaveAsXml(List<ThingABob> data)
        {
            var writer = new StreamWriter(@"../../SampleData.xml");
            XmlSerializer.Serialize(writer, data);
            writer.Close();
        }

        private static List<ThingABob> ReadJson()
        {
            var reader = new StreamReader(@"../../SampleData.json");
            var data = JsonSerializer.ReadObject(reader.BaseStream) as List<ThingABob>;
            return data;
        }

        private static List<ThingABob> ReadXml()
        {
            var reader = new StreamReader(@"../../SampleData.xml");
            var data = XmlSerializer.Deserialize(reader.BaseStream) as List<ThingABob>;
            return data;
        }
    }
}
