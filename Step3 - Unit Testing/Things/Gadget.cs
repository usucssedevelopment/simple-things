﻿using System.Runtime.Serialization;

namespace Things
{
    [DataContract]
    public class Gadget : ThingABob
    {
        [DataMember]
        public double Cost { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public int Width { get; set; }

        public override double ComputeShippingCost()
        {
            double result = 0;
            if (Length > 0 && Width>0)
                result = 1 + Length*Width*.05;
            return result;
        }
    }
}
