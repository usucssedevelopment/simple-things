﻿using System.Runtime.Serialization;

namespace Things
{
    [DataContract]
    public abstract class ThingABob
    {
        [DataMember]
        public int Id { get; set; }

        public abstract double ComputeShippingCost();
    }
}
