﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace Things
{
    public class JsonReaderWriter : IReaderWriter
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(List<ThingABob>),
                     new[] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });


        public void Write(List<ThingABob> data, string filename)
        {
            var writer = new StreamWriter(filename + ".json");
            JsonSerializer.WriteObject(writer.BaseStream, data);
            writer.Close();
        }

        public List<ThingABob> Read(string filename)
        {
            var reader = new StreamReader(filename + ".json");
            var data = JsonSerializer.ReadObject(reader.BaseStream) as List<ThingABob>;
            return data;
        }
    }
}
