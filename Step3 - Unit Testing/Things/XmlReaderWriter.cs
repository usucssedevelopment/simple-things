﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Things
{
    public class XmlReaderWriter : IReaderWriter
    {
        private static readonly XmlSerializer XmlSerializer = new XmlSerializer(typeof(List<ThingABob>),
                        new[] { typeof(ThingABob), typeof(Gadget), typeof(Widget) });

        public void Write(List<ThingABob> data, string filename)
        {
            var writer = new StreamWriter(filename + ".xml");
            XmlSerializer.Serialize(writer, data);
            writer.Close();
        }

        public List<ThingABob> Read(string filename)
        {
            var reader = new StreamReader(filename + ".xml");
            var data = XmlSerializer.Deserialize(reader.BaseStream) as List<ThingABob>;
            return data;
        }
    }
}
