﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Things;

namespace ThingsTesting
{
    [TestClass]
    public class WidgetTester
    {
        [TestMethod]
        public void Widget_TestConstructionAndProperties()
        {
            var widget = new Widget();
            Assert.AreEqual(0, widget.Id);
            Assert.AreEqual(0, widget.Weight);

            widget = new Widget() {Id = 10, Weight = 20};
            Assert.AreEqual(10, widget.Id);
            Assert.AreEqual(20, widget.Weight);

            widget.Id = 11;
            Assert.AreEqual(11, widget.Id);

            widget.Weight = 22;
            Assert.AreEqual(22, widget.Weight);       
        }

        [TestMethod]
        public void Widget_TestShippingCostWithZeroWeight()
        {
            // Test shipping cost when the weight is 0
            var widget = new Widget() { Weight = 0 };
            var shipping = widget.ComputeShippingCost();
            Assert.AreEqual(0, shipping);
        }

        [TestMethod]
        public void Widget_TestShippingCostWithSmallWeightValues()
        {
            // Test shipping cost when the weight is >0 but <=100
            var widget = new Widget() { Weight = 0.01 };
            var shipping = widget.ComputeShippingCost();
            Assert.AreEqual(0.02, shipping);

            widget = new Widget() { Weight = 40 };
            shipping = widget.ComputeShippingCost();
            Assert.AreEqual(0.02, shipping);

            widget = new Widget() { Weight = 100 };
            shipping = widget.ComputeShippingCost();
            Assert.AreEqual(0.02, shipping);
        }

        [TestMethod]
        public void Widget_TestShippingCostWithMediumWeightValues()
        {
            // Test shipping cost when the weight is >100 but <=200

            var widget = new Widget() { Weight = 100.01 };
            var shipping = widget.ComputeShippingCost();
            Assert.AreEqual(0.05, shipping);

            widget = new Widget() { Weight = 165 };
            shipping = widget.ComputeShippingCost();
            Assert.AreEqual(0.05, shipping);

            widget = new Widget() { Weight = 200 };
            shipping = widget.ComputeShippingCost();
            Assert.AreEqual(0.05, shipping);
        }

        [TestMethod]
        public void Widget_TestShippingCostWithHeavyWeightValues()
        {
            // Test shipping cost when the weight is >200

            var widget = new Widget() { Weight = 200.01 };
            var shipping = widget.ComputeShippingCost();
            Assert.AreEqual(200.01*1.5, shipping);
        }


    }
}
