﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Things;

namespace ThingsTesting
{
    [TestClass]
    public class GadgetTester
    {
        [TestMethod]
        public void Gadget_TestConstructionAndProperties()
        {
            var gadget = new Gadget();
            Assert.AreEqual(0, gadget.Id);
            Assert.AreEqual(0, gadget.Length);
            Assert.AreEqual(0, gadget.Width);
            Assert.AreEqual(0, gadget.Cost);

            gadget = new Gadget()
            {
                Id = 10,
                Length = 20,
                Width = 30,
                Cost = 40
            };
            Assert.AreEqual(10, gadget.Id);
            Assert.AreEqual(20, gadget.Length);
            Assert.AreEqual(30, gadget.Width);
            Assert.AreEqual(40, gadget.Cost);

            gadget.Id = 11;
            Assert.AreEqual(11, gadget.Id);

            gadget.Length = 22;
            Assert.AreEqual(22, gadget.Length);

            gadget.Width = 33;
            Assert.AreEqual(33, gadget.Width);

            gadget.Cost = 44;
            Assert.AreEqual(44, gadget.Cost);
        }

        [TestMethod]
        public void Gadget_TestShippingCostWithZeroValues()
        {
            var g = new Gadget() { Length = 0, Width = 0 };
            var shipping = g.ComputeShippingCost();
            Assert.AreEqual(0, shipping);

            g = new Gadget() { Length = 2, Width = 0 };
            shipping = g.ComputeShippingCost();
            Assert.AreEqual(0, shipping);

            g = new Gadget() { Length = 0, Width = 4 };
            shipping = g.ComputeShippingCost();
            Assert.AreEqual(0, shipping);
        }

        [TestMethod]
        public void Gadget_TestShippingCostNormal()
        {
            var g = new Gadget() { Length = 5, Width = 20 };
            var shipping = g.ComputeShippingCost();
            Assert.AreEqual(6, shipping);
        }

    }
}
