﻿using System.Collections.Generic;

namespace Things
{
    public class ThingABobCollection
    {
        private List<ThingABob> _myCollection = new List<ThingABob>();

        public void Add(ThingABob thingy) { _myCollection.Add(thingy); }
        public int Count => _myCollection.Count;
        public IReaderWriter MyImporterExporter { get; set; }

        public void Write(string filename)
        {
            MyImporterExporter?.Write(_myCollection, filename);
        }

        public void Read(string filename)
        {
            _myCollection = MyImporterExporter?.Read(filename);
        }

    }
}
